import React, { Component } from 'react';
import { connect } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import { login } from '../actions/user';
import { routes } from '../config/routes';
import { ts } from '../helpers/funs';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ""
        };
    }

    componentWillMount() {
        const localStorIsLoggedIn = localStorage.getItem('isLoggedIn');
        if (typeof localStorIsLoggedIn !== 'undefined' && localStorIsLoggedIn === 'true') {
            this.props.history.push(routes.HOME);
        }
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <form method="POST" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="email">Email address:</label>
                                <input type="email" className="form-control" id="email" name="email" onChange={this.handleChange} />
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </Col>
                </Row>
            </Container>
        );
    }

    handleChange = (e) => {
        const { target: { name, value } } = e;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { email } = this.state;
        const { dispatch, history } = this.props;
        dispatch(login(email));
        history.push(routes.HOME);
        ts(`Welcome ${email}`);
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

export default connect(mapStateToProps)(Login);