import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="container-fluid text-center">
                <p>This is just a demo in React, Redux which is bootstrapped via Create-react-app</p>
            </footer>
        );
    }
}

export default Footer;